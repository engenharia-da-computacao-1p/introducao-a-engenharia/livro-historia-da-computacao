# Introdução a Engenharia de Computação

Este livro está sendo escrito pelos alunos do Curso de Engenharia da Computação da Universidade Tecnológica do Paraná. O livro é faz parte da disciplina de Introdução a Engenharia e os alunos que participaram da edição estão [listados abaixo](#Autores).

## Índice

1. [Introdução a Engenharia](capitulos/introduçãoaengenharia.md)
2. [Computadores Mecânicos](capitulos/computadores_mecanicos.md)
3. [Computadores Eletrônicos](capitulos/computadores_eletronicos.md)
4. [Computação Quântica](capitulos/computacao_quantica.md)
5. [Linguagem de Programação](capitulos/linguagensdeprogramacao.md)
6. [A Internet](capitulos/ainternet.md)

## Autores
Esse livro foi escrito por:

| Avatar | Nome | Nickname | Email | Instagram | Twitter |
| ------ | ---- | -------- | ----- | --------- | --------|
| ![](https://gitlab.com/uploads/-/system/user/avatar/9983291/avatar.png?width=400)  | Matheus Ferreira | matheusfes | [matheusespindola@alunos.utfpr.edu.br](mailto:matheusespindola@alunos.utfpr.edu.br)| @matheusf.es | @matheusf_es |
