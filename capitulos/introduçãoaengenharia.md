[ ⬅ Voltar ](.../README.MD)

## Introdução a Engenharia

Os engenheiros são indivíduos que combinam conhecimentos da ciência, da matemática e da economia para solucionar problemas técnicos com os quais a sociedade se depara. É o conhecimento prático que distingue os engenheiros dos cientistas, que também são mestres da ciência e da matemática. 

<p><img src="https://gitlab.com/engenharia-da-computacao-1p/introducao-a-engenharia/livro-historia-da-computacao/-/raw/master/imagens/engenheiro.jpg" width = 500px height = 300></a></p>

### Engenharia da Computação 

A Engenharia de Computação é o ramo das engenharias que forma profissionais habilitados para o desenvolvimento de hardware e software. O engenheiro de computação desenvolve programas, circuitos eletrônicos, computadores e seus acessórios, além de gerenciar redes e sistemas. O curso tem duração média de 5 anos onde a grade de matérias são Álgebra Linear, Cálculo, Física, Geometria Analítica.

<p><img src="https://gitlab.com/engenharia-da-computacao-1p/introducao-a-engenharia/livro-historia-da-computacao/-/raw/master/imagens/engenheirodacomputa%C3%A7%C3%A3o.jpg" width = 500px height = 300></a></p>

### Diferença entre Engenharia de Computação e Ciência da Computação

Tanto Ciência da Computação quanto Engenharia de Computação têm a matemática e a computação como seus principais pilares. Porém, na Ciência da Computação foca-se no desenvolvimento de soluções por meio de sistemas de software, enquanto que na Engenharia de Computação foca-se no desenvolvimento de soluções que envolvem aspectos tanto relacionados à elétrica/eletrônica quanto ao software. Assim, se você gosta de computação, mas tem dificuldades ou não tem muita afinidade com elétrica/eletrônica, provavelmente vai se dar melhor na Ciência da Computação. Mas, se você gosta muito de computação e tem afinidade com elétrica/eletrônica, provavelmente vai se identificar melhor com a Engenharia de Computação.

[ ⬅ Voltar ](.../README.MD)
