[ ⬅ Voltar ](.../README.MD)

## Computação Quântica

A computação quântica é a ciência que estuda as aplicações das teorias e propriedades da mecânica quântica na Ciência da Computação. Dessa forma seu principal foco é o desenvolvimento do computador quântico.

Na computação clássica o computador é baseado na arquitetura de Von Neumann que faz uma distinção clara entre elementos de processamento e armazenamento de dados, isto é, possui processador e memória destacados por um barramento de comunicação, sendo seu processamento sequencial.

<img src="https://gitlab.com/engenharia-da-computacao-1p/introducao-a-engenharia/livro-historia-da-computacao/-/raw/master/imagens/computa%C3%A7%C3%A3o-qu%C3%A2ntica.png" width="500">

Entretanto os computadores atuais possuem limitações, como por exemplo na área de Inteligência Artificial (IA), onde não existem computadores com potência ou velocidade de processamento suficiente para suportar uma IA avançada. Dessa forma surgiu a necessidade da criação de um computador alternativo dos usuais que resolvesse problemas de IA, ou outros como a fatoração em primos de números muito grandes, logaritmos discretos e simulação de problemas da Física Quântica.

A Lei de Moore afirma que a velocidade de um computador é dobrada a cada 12 meses. Assim sempre houve um crescimento constante na velocidade de processamento dos computadores. Entretanto essa evolução tem um certo limite, um ponto onde não será possível aumentar essa velocidade e então se fez necessária uma revolução significativa na computação para que este obstáculo fosse quebrado. E assim os estudos em Computação Quântica se tornaram muito importantes e a necessidade do desenvolvimento de uma máquina extremamente eficiente se torna maior a cada dia.

Na computação clássica, um bit é uma peça única de informação que pode existir em dois estados: 1 ou 0. Por isso, é chamado de binário. Na computação quântica, o bit é substituído pelo “qubit” (quantum bit), que também têm dois estados. No entanto, diferentemente dos bits, os qubits podem armazenar muito mais informação que apenas 1 ou 0. Pois podem existir em qualquer superposição desses valores.

<img src="https://gitlab.com/engenharia-da-computacao-1p/introducao-a-engenharia/livro-historia-da-computacao/-/raw/master/imagens/computador-quantico-ibm.webp" width="500">

- IBM Q, o computador quântico de 20 qubits da IBM, apresentado em janeiro de 2019.


##### O que um computador quântico pode fazer que outros não possam?

Como você deve ter percebido, a computação quântica opera com princípios totalmente diferentes daqueles de computadores existentes. Isso faz com que os novos sistemas sejam potencialmente capazes de resolver problemas matemáticos bem específicos, como encontrar números primos imensos.

Os sistemas criptográficos de computação quântica também tendem a ser imensamente mais seguros que seus modelos análogos atuais.

# Referências:

1. Wikipedia, Computador quântico em: <https://pt.wikipedia.org/wiki/Computador_qu%C3%A2ntico>, 2016.

2. HELERBROCK, Rafael. "O que é computação quântica?"; Brasil Escola. Disponível em: <https://brasilescola.uol.com.br/o-que-e/fisica/o-que-e-computacao-quantica.htm>, 2021.

3. Runrun. A computação quântica vem aí. Conheça a tecnologia e prepare-se para ela. Disponível em: <https://blog.runrun.it/computacao-quantica/>, 2021.

4. Neofeed, Cezar Taurion. Como se preparar para a revolução da computação quântica?. Disponível em: <https://neofeed.com.br/blog/home/como-se-preparar-para-a-revolucao-da-computacao-quantica/>, 2021.

[ ⬅ Voltar ](.../README.MD)
