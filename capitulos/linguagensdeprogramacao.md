[ ⬅ Voltar ](.../README.MD)

# Linguagem de Programação

<p><img src="https://gitlab.com/engenharia-da-computacao-1p/introducao-a-engenharia/livro-historia-da-computacao/-/raw/master/imagens/As-15-principais-linguagens-de-programação-no-mundo.png" width = 700px height = 300></a></p>

 Linguagem de programação é uma linguagem formal que, através de uma série de instruções, permite que um programador escreva um conjunto de ordens, ações consecutivas, dados e algoritmos para criar programas que controlam o comportamento físico e lógico de uma máquina.

Programador e máquina se comunicam por meio dessa linguagem, permitindo especificar, com precisão, aspectos como:

- quais dados um software deve operar;
- como esses dados devem ser armazenados ou transmitidos;
- quais ações o software deve executar, de acordo com cada circunstância variável.

Basicamente a linguagem de programação é um sistema de comunicação estruturado, composto por conjuntos de símbolos, palavras-chave, regras semânticas e sintáticas que permitem o entendimento entre um programador e uma máquina.

### Tipos de Linguagem de Programação

As linguagem de programação são classificados em dois tipos principais: linguagem de baixo e alto nível.

 Linguagem de programação de baixo nível são linguagens totalmente orientadas à máquina. Esse idioma serve como uma interface e cria um link inseparável entre hardware e software.
Além disso, exerce controle direto sobre o equipamento e sua estrutura física. Para aplicá-la adequadamente, é necessário que o programador conheça muito bem o hardware. Essa categoria pode ser subdividido em dois tipos.

##### Linguagem de Máquina
É o mais primitivo dos idiomas e é uma coleção de dígitos ou bits binários (0 e 1) que o computador lê e interpreta e é o único idioma que os computadores entendem.

Exemplo: 10110000 01100001

##### Linguagem Assembly
A linguagem Assembly é a primeira tentativa de substituir a linguagem de máquina por uma mais próxima da usada por seres humanos.
Um programa escrito nessa linguagem é armazenado como texto e consiste em uma série de instruções que correspondem ao fluxo de pedidos executáveis ​​por um microprocessador.
No entanto, essas máquinas não entendem a linguagem Assembly. Portanto, devem ser convertidas em linguagem de máquina por meio de um programa chamado Assembler.
Ele gera códigos compactos, rápidos e eficientes criados pelo programador que tem controle total da máquina.

Exemplo: MOV AL, 61h (atribui o valor hexadecimal 61 ao registro “AL”)

As linguagem de programação de alto nível visam facilitar o trabalho do programador, pois usam instruções que são mais fáceis de serem entendidas.
Além disso, a linguagem de alto nível permite que você escreva códigos usando os idiomas que conhece (português, espanhol, inglês etc.) traduzindo-os em seguida para o idioma da máquina por tradutores ou compiladores.

##### Tradutor
Eles traduzem programas escritos em uma linguagem de programação para a linguagem de máquina do computador e são executados à medida que são traduzidos.

##### Compilador
Ele permite que você traduza um programa inteiro de uma só vez, tornando-o mais rápido e pode ser armazenado para uso posterior sem a necessidade de uma nova tradução.

# Referências:

1. Kenzie – Linguagem de programação: o que é e qual linguagem aprender. Disponível em <https://kenzie.com.br/blog/linguagem-de-programacao/>, 2019.


[ ⬅ Voltar ](.../README.MD)
