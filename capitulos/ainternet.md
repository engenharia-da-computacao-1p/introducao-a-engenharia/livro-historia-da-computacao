[ ⬅ Voltar ](.../README.MD)

## A Internet

A internet surgiu lá na época da Guerra Fria (1947 – 1991). Foi desenvolvida pelos norte-americanos com o intuito de se comunicarem com seu exército durante a guerra caso os meios de comunicação tradicionais da época fossem destruídos em ataques.

Entre 1970 e 1980, a internet deixou de ser uma ferramenta usada somente pelo governo e passou a ser utilizada para fins acadêmicos. A partir de 1990, começou a ser usada pela população em geral, através de serviços de empresas que começaram a oferecer conexão de internet empresarial e residencial.

<p><img src="https://gitlab.com/engenharia-da-computacao-1p/introducao-a-engenharia/livro-historia-da-computacao/-/raw/master/imagens/ainternet.jpg" width = 500px height = 300></a></p>

A internet um sistema global de redes de computadores interligadas que utilizam um conjunto próprio de protocolos (Internet Protocol Suite ou TCP/IP) com o propósito de servir progressivamente usuários no mundo inteiro.

# Referências:

1. Wikipedia – Internet. Disponível em <https://pt.wikipedia.org/wiki/Internet>, 2016.

2. Copeltelecom – O que é internet?. Disponível em <https://www.copeltelecom.com/site/blog/o-que-e-internet/>.

[ ⬅ Voltar ](.../README.MD)
