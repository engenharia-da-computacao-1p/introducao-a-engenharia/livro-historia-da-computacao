[ ⬅ Voltar ](.../README.MD)

# Computadores Eletrônicos 

Os computadores da primeira geração foram criados entre os anos de 1950 a 1960 baseado em uso de relé e válvulas eletrônicas para realização de cálculos. Devido ao seu grande consumo de energia os computadores da primeira geração sofriam muitos problemas, tendo pouca durabilidade, além de precisar ser reprogramado a cada tarefa. Estas máquinas programavam-se diretamente em código máquina e eram capazes de realizar até 1000 instruções por segundo.

O período da segunda geração foi de 1959 a 1965. A esta geração os transistores foram usadas e que eram mais baratos, consumir menos energia, mais compacto no tamanho, mais confiável e mais rápido do que a primeira geração as máquinas feitas de tubos de vácuo. Nesta geração, núcleos magnéticos foram usadas como memória principal e fitas magnéticas e discos magnéticos como dispositivos de armazenamento secundário.

O período da terceira geração foi de 1965 a 1971. Os computadores de terceira geração utilizados circuitos integrados (IC's) em lugar de transistores. Um único IC tem vários transistores, resistores e capacitores junto com o circuitos associados. O IC foi inventado por Jack Kilby. Esta evolução fez computadores menores em tamanho, confiável e eficiente. A esta geração processamento remoto, time-sharing, multi-programação sistema operacional foram utilizados.

O período da quarta geração foi de 1971 a 1980. Os computadores de quarta geração utilizadas de forma Very Large Scale Integrated (VLSI) circuitos. Circuitos VLSI ter cerca de 5.000 transistores e de outros elementos de circuito e os seus associados os circuitos em um único chip, é possível que os microcomputadores de quarta geração. Quarta geração os computadores tornaram-se mais potente e compacto, confiável e acessível. Como um resultado, que deu origem ao personal computer (PC) revolução.

O período de quinta geração é de 1980 até os dias atuais. Na quinta geração, a VLSI technology se tornou ULSI (Ultra Large Scale Integration) tecnologia, resultando na produção de chips microprocessadores tendo dez milhões os componentes eletrônicos. Esta geração é baseada em hardware de processamento paralelo e a IA (Inteligência Artificial) software. AI é um novo ramo da ciência da computação, que interpreta meio e método de fazer computadores pensar como seres humanos.

## Primeira Geração

Em junho de 1943, durante a Segunda Guerra Mundial, o exército norte-americano assinou o contrato de construção do ENIAC. Na época, ele custou por volta de 500 mil dólares, o que hoje equivaleria a mais de 10 milhões de dólares. Essa máquina tinha como principal função calcular as trajetórias balísticas do lançamento de mísseis, tarefa muito trabalhosa até então, pois era feita à mão.
Depois de um tempo em funcionamento, o ENIAC foi utilizado também para realizar os cálculos da construção da primeira bomba de hidrogênio.

| ENIAC | 
| --------------------- |
| <p><img src="https://gitlab.com/engenharia-da-computacao-1p/introducao-a-engenharia/livro-historia-da-computacao/-/raw/master/imagens/ENIAC.gif" width = 200px></a></p> | 
| Apresentado em 1946, este computador ocupava uma área superiora 170 m2, pesava 30 toneladas, utilizando 18.000 válvulas e 10.000 capacitores, consumindo 150.000 watts de energia e custando vários milhões de dólares. Além de todo este tamanho, sua preparação demorava semanas, pois a programação era realizada pela ligação de fios. |

## Segunda Geração

O IBM 7030, também conhecido por Strech, foi o primeiro supercomputador lançado na segunda geração, desenvolvido pela IBM. Seu tamanho era bem reduzido comparado com máquinas como o ENIAC, podendo ocupar somente uma sala comum. Ele era utilizado por grandes companhias, custando em torno de 13 milhões de dólares na época.

| IBM 7030 | 
| --------------------- |
| <p><img src="https://gitlab.com/engenharia-da-computacao-1p/introducao-a-engenharia/livro-historia-da-computacao/-/raw/master/imagens/IBM7030.jpg" width = 200px height = 150px></a></p> | 
| Esta máquina executava cálculos na casa dos microssegundos, o que permitia até um milhão de operações por segundo. O IBM 7030 promoveu avanços em muitas das principais tecnologias de computador, incluindo memória principal, design de circuitos de transistor e embalagem de circuitos. |

## Terceira Geração

O Modelo 168 ganhou seu poder em grande parte de uma combinação de armazenamento de buffer de ultra-alta velocidade, operação dos circuitos lógicos da unidade de instrução, que capitalizou na disponibilidade de instruções e dados do buffer, e um alto grau de simultaneidade na operação. 

| System/370 Model 168 | 
| --------------------- |
| <p><img src="https://gitlab.com/engenharia-da-computacao-1p/introducao-a-engenharia/livro-historia-da-computacao/-/raw/master/imagens/System370.jpg" width = 200px height = 150px></a></p> | 
| A operação da unidade de instrução foi sobreposta, permitindo que até três instruções fossem preparadas simultaneamente, de modo que a próxima instrução sequenciada do programa estivesse pronta para execução. Introduzido com o 168 foram um registro adicional e buffer de instrução para aumentar a sobreposição e reorganização do buffer opcional de 16K para ganhar velocidade. |

## Quarta Geração

O Kenbak-1 era bastante rústico, o sistema usava uma coleção de interruptores como mecanismo de entrada de dados e era mais análogo a um mainframe industrial do que a um desktop contemporâneo.

| Kenbak-1 | 
| --------------------- |
| <p><img src="https://gitlab.com/engenharia-da-computacao-1p/introducao-a-engenharia/livro-historia-da-computacao/-/raw/master/imagens/kenbak1.jpg" width = 200px></a></p> | 
|  O Kenbak-1 foi inventado antes do primeiro microprocessador, não possuía a CPU num único chip, mas era baseada em vários CIs distintos de lógica TTL. Máquina de 8 bits, apresentava 256 bytes de memória RAM (≈1/4000 megabyte). O ciclo de instrução de máquina era de 1 microssegundo (equivalente a uma freqüência de clock de 1 Mhz). A programação do Kenbak-1 era feita diretamente em código de máquina, ajustando-se vários botões e chaves. Havia uma ranhura na parte superior à direita do painel, projetada para abrigar um leitor de cartões, mas isso nunca chegou a ser desenvolvido. |

O Apple II foi o primeiro computador pessoal a popularizar-se junto do público em geral e um dos primeiros a oferecerem gráficos coloridos. Descendente do Apple I, o primeiro computador concebido por Stephan Wozniak, que havia sido produzido em número muito limitado e vendido na forma de um kit para montar, o Apple II nasceu da combinação dos talentos de Stephan Wozniak e Steve Jobs, então sócios na recém-formada Apple.

| Apple II | 
| --------------------- |
| <p><img src="https://gitlab.com/engenharia-da-computacao-1p/introducao-a-engenharia/livro-historia-da-computacao/-/raw/master/imagens/apple2.png" width = 200px height = 147px></a></p> | 
| Vinha equipado com um processador MOS Technology 6502 a 1 MHz, interface para cassetes áudio que permitia gravar e carregar os programas, uma saída de vídeo composto que permitia a exibição de gráficos até 16 cores, placa de som de um canal, teclado (apenas para maiúsculas), oito slots de expansão internas, um interpretador de BASIC integrado na ROM de 12 KB e duas possibilidades diferentes para a memória RAM: o utilizador podia escolher entre um modelo com 4 KB ou um com 48 KB de memória, sendo que o primeiro permitia a expansão da memória em módulos de 4 ou 16 KB até ao máximo dos 48 KB. |

## Quinta Geração

Lançado a 12 de agosto de 1981, o IBM Personal Computer, ou IBM PC, foi desenvolvido ao longo de um ano por uma equipa de 12 engenheiros e projetistas da IBM.

| IBM PC | 
| --------------------- |
| <p><img src="https://gitlab.com/engenharia-da-computacao-1p/introducao-a-engenharia/livro-historia-da-computacao/-/raw/master/imagens/IBMPC.jpg" width = 200px height = 147px></a></p> | 
| Trazia um processador Intel 8088 rodando a 4,77 MHz, associado a 16 KB de RAM. A versão básica incluía ainda um adaptador gráfico preto e branco e o teclado com 83 teclas. O comprador podia "turbinar" o IBM PC com unidades de disquete, pentes de até 256 KB de RAM, adaptador gráfico colorido e uma impressora Epson MX-80. Também eram vendidos à parte o sistema operacional (Microsoft DOS), pacotes de aplicativos e o monitor com 16 cores, além de outros acessórios. |

# Referências:

1. Porto Editora – Apple II na Infopédia. Porto: Porto Editora. Disponível em <https://www.infopedia.pt/$apple-ii>, 2016.

2. BROOKSHEAR, J. Glenn. Ciência da computação: Uma visão abrangente. 7. ed. Porto Alegre: Bookman Companhia Editora, 2005.

3. André Luiz Dias Gonçalves. Primeiro PC da IBM foi lançado há 40 anos em: <https://www.tecmundo.com.br/produto/222975-primeiro-pc-ibm-lancado-ha-40-anos.htm>, 2016.

4. INFORMÁTICA, Museu Virtual da. Computadores para cálculo científico. Disponível em: <http://piano.dsi.uminho.pt/museuv/1946hmark1.html>, 2016.

5. Pioneer Programmer: Jean Jennings Bartik and the Computer that Changed the World de Jean Jennings Bartik Livro presente no Google Books, 2017.

6. Educação e Novas Tecnologias de GLAUCIA DA SILVA BRITO Livro presente no Google Books, 2017.

7. História da informática (Parte 6: Sistemas embarcados e supercomputadores), 2012.

8. História da Computação de Raul Sidnei Wazlawick Livro presente no Google Books, 2019.

9. Bit by Bit: An Illustrated History of Computers de Stan Augartenm, 2019.

10. Porto Editora – Apple II na Infopédia. Porto: Porto Editora. Disponível em <https://www.infopedia.pt/$apple-ii>, 2016.

[ ⬅ Voltar ](.../README.MD)
