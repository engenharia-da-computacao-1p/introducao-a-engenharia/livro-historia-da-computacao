[ ⬅ Voltar ](.../README.MD)

# Computadores Mecânicos

Computadores Mecânicos usavam componentes como polias, alavancas e engrenagens. Eles se dividem em duas sub-categorias, os não-programáveis (o que os definiria não como computadores, mas como "máquinas", mas acabam sendo considerados dada a sua complexidade) e os programáveis.

Os computadores mecânicos não-programáveis eram aparatos desenvolvidos para uma função ou conjunto de funções específicas.

| Máquina de Anticítera | 
| --------------------- |
| ![](https://gitlab.com/engenharia-da-computacao-1p/introducao-a-engenharia/livro-historia-da-computacao/-/raw/master/imagens/maquinadeanticitera.jpg) | 
| A máquina de Anticítera é o computador analógico e planetário mais antigo que se conhece, criado no século I a.C. na Grécia romana. Era usado para prever posições astronômicas e eclipses, como função de calendário e astrologia. Ela também podia rastrear o ciclo de quatro anos dos jogos atléticos que eram parecidos, mas não idênticos, a uma Olimpíada, o ciclo dos Jogos Olímpicos da Antiguidade. |

No século XVII, as pesquisas do matemático John Napier sobre logaritmos resultaram em um dispositivo conhecido por “Ossos de Napier”

|    Ossos de Napier    |
| --------------------- |
| ![](https://gitlab.com/engenharia-da-computacao-1p/introducao-a-engenharia/livro-historia-da-computacao/-/raw/master/imagens/ossosdenapier.png) |
| Os ossos de Napier é um dispositivo de cálculo de funcionamento manual criado por John Napier de Merchiston para cálculo de produtos e quocientes de números. Usando as tabelas de multiplicação incorporadas nas hastes, a multiplicação e a divisão podem ser reduzidas a operações de adição e subtração. O uso mais avançado das hastes pode até extrair raízes quadradas . |

Em 1642, o matemático Blaise Pascal, com 20 anos, projetou uma máquina que é considerada a primeira calculadora mecânica: a Máquina de Pascal.
A máquina de Pascal era capaz de realizar somas e subtrações utilizando um sistema de engrenagens. Usuários mais avançados conseguiam realizar multiplicações e divisões realizando cálculos sucessivos, porém, era um método bastante trabalhoso.

|   Máquina de Pascal   |
| --------------------- |
| ![](https://gitlab.com/engenharia-da-computacao-1p/introducao-a-engenharia/livro-historia-da-computacao/-/raw/master/imagens/maquinadepascal.jpg) | 
| A máquina contém como elemento essencial uma roda dentada construída com 10 "dentes". Cada "dente" corresponde a um algarismo, de 0 a 9.  A primeira roda da direita corresponde às unidades, a imediatamente à sua esquerda corresponde às dezenas, a seguinte às centenas e assim sucessivamente. |
  
Em uma fábrica de tecidos, o costureiro Joseph Jacquard visando uma maneira de facilitar o processo de criação de diferentes padrões de tecelagem, desenvolve uma máquina na qual o usuário poderia programar a máquina para costurar diferentes desenhos para os tecidos. Bastava que um cartão perfurado, que continha a informação do padrão desejado, fosse inserido no dispositivo. Uma ideia revolucionária, que foi nomeada de Tear programável 

|    Tear Programável   |
| --------------------- |
| ![](https://gitlab.com/engenharia-da-computacao-1p/introducao-a-engenharia/livro-historia-da-computacao/-/raw/master/imagens/tearprogramavel.jpg) |
|  Ela aceitava cartões perfuráveis com entrada do sistema. Dessa maneira, Jacquard Perfurava o cartão com o desenho desejado e a máquina o reproduzia no tecido. |

Após um período, em 1837, Babbage lançou uma nova máquina que era capaz de calcular funções de diversas naturezas (trigonometria, logaritmos) de forma muito simples. chamada de Engenho Analítico (Máquina Analítica), que aproveitava todos os conceitos do Tear Programável, como o uso dos cartões. Além disso, instruções e comandos podiam ser informados pelos cartões, fazendo uso de registradores primitivos. A precisão chegava a 50 casas decimais.

|   Maquina Analítica   |
| --------------------- |
| ![](https://gitlab.com/engenharia-da-computacao-1p/introducao-a-engenharia/livro-historia-da-computacao/-/raw/master/imagens/maquinaanalitica.png) |
|  A maquina analítica incorporava uma unidade lógica aritmética, fluxo de controle na forma de ramificações condicionais, loops e memória integrada, tornando-o o primeiro projeto para um computador de uso geral que poderia ser descrito em termos modernos como Turing-complete. |

# Referências:

1. BROOKSHEAR, J. Glenn. Ciência da computação: Uma visão abrangente. 7. ed. Porto Alegre: Bookman Companhia Editora, 2005.

2. UEM. Máquina de Pascal. Disponível em: <http://www.din.uem.br/museu/virtualhtml/600_maquina.htm>, 2016.

3. INFORMÁTICA, Museu Virtual da. Computadores para cálculo científico. Disponível em: <http://piano.dsi.uminho.pt/museuv/1946hmark1.html>, 2016.

4. WIKIPEDIA. Ossos de Napier. Disponível em: <https://pt.wikipedia.org/wiki/Ossos_de_Napier>, 2016.

[ ⬅ Voltar ](.../README.MD)
